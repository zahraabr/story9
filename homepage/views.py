import json
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import *

# Create your views here.
def index(request):
    return render(request, 'index.html')

@csrf_exempt
def likes(request):
    data = json.loads(request.body)
    try:
        book_data = Book.objects.get(book_id=data['id'])
        book_data.like += 1
        book_data.save()
    except:
        book_data = Book.objects.create(
            book_id = data['id'],
            image_link = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            published_date = data['volumeInfo']['publishedDate'],
            like = 1
        )
    return JsonResponse({
        'like' : book_data.like
    })

@csrf_exempt
def unlike(request):
    data = json.loads(request.body)
    try:
        book_data = Book.objects.get(book_id=data['id'])
        if book_data.like > 0:
            book_data.like -= 1
        book_data.save()
    except:
        book_data = Book.objects.create(
            book_id = data['id'],
            image_link = data['volumeInfo']['imageLinks']['smallThumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            publisher = data['volumeInfo']['publisher'],
            published_date = data['volumeInfo']['publishedDate'],
        )
    return JsonResponse({
        'like' : book_data.like
    })

def top5books(request):
    book = Book.objects.order_by('-like')

    list_of_books = []
    for i in book:
        list_of_books.append({
            'image_link': i.image_link,
            'title': i.title,
            'authors': i.authors,
            'publisher': i.publisher,
            'published_date': i.published_date,
            'like': i.like,
        })
    
    return JsonResponse({
        'list_of_books' : list_of_books
    })
