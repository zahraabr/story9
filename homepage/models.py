from django.db import models

# Create your models here.
class Book(models.Model):
    book_id = models.CharField(max_length=128, unique=True)
    image_link = models.TextField()
    title = models.CharField(max_length=255)
    authors = models.CharField(max_length=255, null=True)
    publisher = models.CharField(max_length=255, null=True)
    published_date = models.CharField(max_length=255, null=True)
    like = models.IntegerField(default=0)

    def __str__(self):
        return self.title