from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('likes/', views.likes, name='likes'),
    path('unlike/', views.unlike, name='unlike'),
    path('top5books/', views.top5books, name='top5books'),
]