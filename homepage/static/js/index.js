var books = [];

$(document).ready(function() {
    $("#input").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        var xhttprequest = new XMLHttpRequest();
        console.log(q)
        xhttprequest.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                var data = JSON.parse(this.responseText);
                books = data.items;
                $('#content').html('')
                var result = '<tr>';
                for (var i = 0; i < data.items.length; i++) {
                    result += "<tr> <th scope='row' class='table-heading align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='table-heading align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='table-heading align-middle'>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td class='table-heading align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" +
                        "<td class='table-heading align-middle'>" + data.items[i].volumeInfo.publishedDate + "</td>" +
                        "<td class='table-heading align-middle'>" + 
                            "<button type='button' onclick='like("+ i +")' id='likebutton" + i + "'>like</button>" + 
                            "<button type='button' onclick='unlike("+ i +")' id='unlikebutton" + i + "'>unlike</button>" + 
                            "<p id='numOfLikes" + i + "' data-value='0'>0</p>" +
                        "</td>"
                }
                $('#content').append(result);
            }
        }
        xhttprequest.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + q, true);
        xhttprequest.send();
    });
});

function like(i) {
    var data = JSON.stringify(books[i]);
    var xhttprequest= new XMLHttpRequest();
    xhttprequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var liked = JSON.parse(this.responseText).like;
            $("#numOfLikes"+i).html(liked);
        }
    }
    xhttprequest.open("POST", "/likes/");
    xhttprequest.send(data);
}

function unlike(i) {
    var data = JSON.stringify(books[i]);
    var xhttprequest= new XMLHttpRequest();
    xhttprequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var liked = JSON.parse(this.responseText).like;
            $("#numOfLikes"+i).html(liked);
        }
    }
    xhttprequest.open("POST", "/unlike/");
    xhttprequest.send(data);
}

function top5books() {
    var xhttprequest = new XMLHttpRequest();
    xhttprequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var list_of_books = JSON.parse(this.responseText).list_of_books;
            $('.modal-body').html('');
            for(var i = 0; i < 5; i++) {
                $('.modal-body').append(
                    "<p>" + list_of_books[i]['title'] + "</p>" +
                    "<p>" + list_of_books[i]['authors'] + "</p>" +
                    "<p>" + list_of_books[i]['like'] + "</p>"
                )
            }
        }
    }
    xhttprequest.open("GET", "/top5books/", true);
    xhttprequest.send();

}