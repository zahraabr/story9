from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve, reverse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from django.conf import settings
from importlib import import_module
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


import random
import string
import time


# Create your tests here.
class Story9Test(TestCase):
    def test_URL_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_template_is_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model(self):
        letters = string.ascii_letters
        random_book_id = ''.join(random.choice(letters) for i in range(10))
        random_book_id_2 = ''.join(random.choice(letters) for i in range(10))
        random_like_count = random.randint(1,10)
        title = 'This is a book'
        authors = None
        publisher = None
        publishedDate = None

        Book.objects.create(
            book_id = random_book_id_2,
            image_link = '',
            title = title,
            authors = authors,
            publisher = publisher,
            published_date = publishedDate,
            like = random_like_count,
        )

        data = Book.objects

        try:
            data.get(book_id=random_book_id)
        except:
            Book.objects.create(
                book_id = random_book_id,
                image_link = '',
                title = 'A random title',
                authors = authors,
                publisher = publisher,
                published_date = publishedDate,
                like = random_like_count,
            )

        self.assertEqual(data.all().count(),2)
        self.assertEqual(str(data.get(title=title)), title)

class Story9FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        # super().setUp()
        # chrome_options = webdriver.ChromeOptions()
        # self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story9FunctionalTest, self).setUp()

    def tearDown(self) :
        self.driver.quit()
        super().tearDown()

    def test_when_searching_for_a_book_then_it_will_return_a_table_of_books(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        searched_book = 'Dynamic Web Programming and HTML5'
        self.driver.find_element_by_id('input').send_keys(searched_book)

        time.sleep(15)

        response_content = self.driver.page_source
        self.assertIn('Dynamic Web Programming and HTML5', response_content)
        self.assertIn('Paul S. Wang', response_content)
        self.assertIn('CRC Press', response_content)
        self.assertIn('2012-11-21', response_content)

    def test_when_like_button_is_click_then_it_increment_the_number_of_likes(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        searched_book = 'Dynamic Web Programming and HTML5'
        for i in searched_book:
            self.driver.find_element_by_id('input').send_keys(i)
            time.sleep(0.1)

        time.sleep(5)

        response_content = self.driver.page_source
        self.assertIn('like', response_content)
        self.assertIn('unlike', response_content)

        self.driver.find_element_by_id('likebutton0').click()

        response_content = self.driver.page_source
        self.assertIn('1', response_content)
    
    def test_when_unlike_button_is_click_then_it_decrement_the_number_of_likes(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        searched_book = 'Dynamic Web Programming and HTML5'
        for i in searched_book:
            self.driver.find_element_by_id('input').send_keys(i)
            time.sleep(0.1)
        
        time.sleep(5)

        response_content = self.driver.page_source
        self.assertIn('like', response_content)
        self.assertIn('unlike', response_content)

        self.driver.find_element_by_id('likebutton0').click()
        self.driver.find_element_by_id('unlikebutton0').click()

        response_content = self.driver.page_source
        self.assertIn('0', response_content)

    def test_when_top_5_books_button_is_click_then_it_will_show_top_5_books_with_the_most_likes(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        searched_book = 'Dynamic Web Programming and HTML5'
        for i in searched_book:
            self.driver.find_element_by_id('input').send_keys(i)
            time.sleep(0.1)
        
        time.sleep(5)

        for i in range(10):
            self.driver.find_element_by_id('likebutton0').click()

        self.driver.find_element_by_id('top5books').click()

        response_content = self.driver.page_source

        self.assertIn('Dynamic Web Programming and HTML5', response_content)
        self.assertIn('Paul S. Wang', response_content)
        self.assertIn('10', response_content)